{-# LANGUAGE CPP #-}
-- Two-stage demultiplexing.
--
-- We assume we know the list of i7 and i5 index oligos.  We seek to
-- decompose a set of reads into a mix of pairs of these by the Maximum
-- Likelihood method.  Once that's done, an empirical Bayesian Maximum
-- Posterior call is done.  All kinds of errors can be rolled into one
-- quality score.
--
--  - Input layer to gather index sequences.
--  - Input layer to gather read group defs.
--  - First pass to gather data.  Any index read shall be represented
--    in a single Word64.  (Reading BAM is slow.  BCL would be
--    much more suitable here.)
--  - Multiple passes of the EM algorithm.
--  - Start with a naive mix, to avoid arguments.
--  - Final calling pass from BAM to BAM.  (BCL to BAM would be nicer.)
--  - Auxillary statistics:  composition of the mix, false assignment
--    rates per read group, minimum achievable false assignment rates

import Bio.Bam
import Bio.Prelude
import Bio.Streaming.Vector             ( stream2vectorN )
import Control.Monad.Log
import Data.Aeson                       ( FromJSON(..), withObject, withText, decodeStrict', (.:) )
import Foreign.C.Types                  ( CUInt(..) )
import Foreign.Marshal.Alloc            ( alloca )
import Paths_jivebunny                  ( version, getDataFileName )
import System.Console.GetOpt
import System.Random                    ( randomRIO )

import qualified Bio.Streaming.Prelude              as Q
import qualified Data.ByteString                    as B
import qualified Data.ByteString.Char8              as BS
import qualified Data.HashMap.Strict                as HM
import qualified Data.Text                          as T
import qualified Data.Text.Encoding                 as T
import qualified Data.Text.IO                       as T
import qualified Data.Text.Lazy                     as L hiding ( singleton )
import qualified Data.Text.Lazy.IO                  as L
import qualified Data.Text.Lazy.Builder             as L
import qualified Data.Text.Lazy.Builder.Int         as L
import qualified Data.Text.Lazy.Builder.RealFloat   as L
import qualified Data.Vector                        as V
import qualified Data.Vector.Generic                as VG
import qualified Data.Vector.Algorithms.Intro       as V
import qualified Data.Vector.Storable               as VS
import qualified Data.Vector.Storable.Mutable       as VSM

#if MIN_VERSION_aeson(2,0,0)
import qualified Data.Aeson.Key                     as K ( toText )
import qualified Data.Aeson.KeyMap                  as K ( toList )
#endif

-- | An index sequence must have at most eight bases.  We represent a
-- base and its quality score in a single byte:  the top three bits are
-- the base ("ACGTN" = [0,1,3,2,7]), the lower five bits are the quality,
-- clamped to 31.

newtype Index = Index Word64 deriving (Storable, Eq)

instance Hashable Index where
    hashWithSalt salt (Index x) = hashWithSalt salt x
    hash (Index x) = hash x

instance Show Index where
    show (Index x) = [ "ACTGNNNN" !! fromIntegral b | i <- [56,48..0], let b = (x `shiftR` (i+5)) .&. 0x7 ]
            ++ 'q' : [ chr (fromIntegral q+33)      | i <- [56,48..0], let q = (x `shiftR` i) .&. 0x1F ]

data P a b = P a b deriving Eq

instance (Storable a, Storable b) => Storable (P a b) where
    sizeOf ~(P a b) = sizeOf a + sizeOf b
    alignment ~(P a b) = alignment a + alignment b
    peek p = do a <- peek (castPtr p) ; b <- peekByteOff (castPtr p) (sizeOf a) ; return (P a b)
    poke p (P a b) = poke (castPtr p) a >> pokeByteOff (castPtr p) (sizeOf a) b

fromS :: B.ByteString -> Index
fromS sq = fromSQ sq (B.replicate (B.length sq) 64)

fromSQ :: B.ByteString -> B.ByteString -> Index
fromSQ sq qs = Index . foldl' (\a b -> a `shiftL` 8 .|. fromIntegral b) 0 $
               take 8 $ (++ repeat 0) $
               B.zipWith (\b q -> shiftL (b .&. 0xE) 4 .|. (min 31 $ max 33 q - 33)) sq qs

fromTags :: BamKey -> BamKey -> BamRaw -> Index
fromTags itag qtag br = fromSQ sq  (if B.null qs then "@@@@@@@@" else qs)
  where
    sq = extAsString itag $ unpackBam br
    qs = extAsString qtag $ unpackBam br

gather :: (MonadIO m, PrimMonad m, MonadLog m)
       => Int -> BamMeta -> Stream (Of BamRaw) m () -> m (VS.Vector (P Index Index))
gather num hdr = case hdr_sorting $ meta_hdr hdr of
    Unsorted    -> greedy
    Grouped     -> greedy
    Queryname   -> greedy
    Unknown     -> safe
    Coordinate  -> fair
  where
    greedy = go stream2vectorN "File is unsorted, sampling up to %d records from the beginning.\n"
    fair   = go subsam2vector  "File is sorted, need to sample up to %d from whole file.\n"
    safe   = go subsam2vector  "File might be sorted, need to sample up to %d from whole file.\n"

    go k m s = do logString_ (printf m (showNum num))
                  k num . Q.map (P <$> fromTags "XI" "YI" <*> fromTags "XJ" "YJ")
                        . Q.filter ((\b -> not (isPaired b) || isFirstMate b) . unpackBam)
                        $ progressNum "reading " 0x100000 s


subsam2vector :: (MonadIO m, Storable a) => Int -> Stream (Of a) m () -> m (VS.Vector a)
subsam2vector sz s0 = do
    mv <- liftIO (VSM.new sz)
    go mv 0 s0
  where
    go !mv !i = inspect >=> \case
                  Left      () -> liftIO $ if i < sz then VS.unsafeFreeze $ VSM.take i mv
                                                     else VS.unsafeFreeze mv
                  Right (a:>s) -> do liftIO $ if i < sz then VSM.write mv i a
                                                        else do p <- randomRIO (0,i)
                                                                when (p < sz) $ VSM.write mv p a
                                     go mv (i+1) s

data IndexTab = IndexTab { unique_indices :: VS.Vector Index
                         , canonical_names :: V.Vector T.Text
                         , alias_names :: HM.HashMap T.Text Int }

single_placeholder :: IndexTab
single_placeholder = IndexTab (VS.singleton (fromS "")) (V.singleton "is4") $
                        HM.fromList [ (k,0) | [_,_,k] <- map T.words $ T.lines default_rgs ]

data Both = Both { p7is :: IndexTab, p5is :: IndexTab }

instance FromJSON Both where
    parseJSON = withObject "toplevel object expected" $ \v ->
                          both <$>  ((v .: "p7index") >>= parse_assocs)
                               <*> (((v .: "p5index") >>= parse_assocs) <|> pure [])
      where
        parse_assocs = withObject "association list expected" $ \o ->
                       sequence [ (,) k <$> withText "sequence expected" (return . T.encodeUtf8) v | (k,v) <- objToList o ]

#if MIN_VERSION_aeson(2,0,0)
        objToList = map (first K.toText) . K.toList
#else
        objToList = HM.toList
#endif

        both as7 as5 = Both (canonical as7) (canonical as5)
          where
            canonical pps =
                let hm = HM.toList $ HM.fromListWith (++) [ (fromS v,[k]) | (k,v) <- pps ]
                in IndexTab (VS.fromList $ map fst hm)
                            (V.fromList $ map (head . snd) hm)
                            (HM.fromList $ [ (k,i) | (i, ks) <- zip [0..] (map snd hm), k <- ks ])

data RG = RG { rgid :: B.ByteString
             , rgi7 :: Int
             , rgi5 :: Int
             , tags :: BamOtherShit }

-- | Parses read group defintions from a file.  The file can have
-- optional header lines, the remainder must be a tab-separated table,
-- first column is the read group name, second is the P7 index name,
-- third is the P5 index name (*must* be present), all others are tagged
-- fields just like BAM expects them in the header.
--
-- For integration with a LIMS, something structured like JSON would
-- probably work better, however, absent such a LIMS, tables are easier
-- to come by.

data RgProblem = RgProblem String deriving (Typeable, Show)
instance Exception RgProblem where displayException (RgProblem msg) = msg

readRGdefns :: MonadLog m => HM.HashMap T.Text Int -> HM.HashMap T.Text Int -> T.Text -> m [ RG ]
readRGdefns p7is p5is = fmap catMaybes . mapM repack . filter (not . null) . map T.words
                      . dropWhile ("#" `T.isPrefixOf`) . T.lines
  where
    problem msg = logMsg Error (RgProblem msg) >> pure Nothing

    repack (rg:_) | T.any (\c -> c == '/' || c == ',') rg =
        problem $ "RG name must not contain ',' or '/': " ++ show rg

    repack (rg:p7:p5:tags) = case HM.lookup p7 p7is of
        Nothing -> problem $ "unknown P7 index " ++ show p7
        Just i7 -> case HM.lookup p5 p5is of
            Nothing -> problem $ "unknown P5 index " ++ show p5
            Just i5 -> do tags' <- mapM repack1 tags
                          pure . Just $ RG (T.encodeUtf8 rg) i7 i5 (catMaybes tags')
    repack ws = problem $ "short RG line " ++ show (T.intercalate "\t" ws)

    repack1 w | T.length w > 3 && T.index w 2 == ':'
                    = pure $ Just (fromString [T.index w 0, T.index w 1], T.encodeUtf8 $ T.drop 3 w)
              | otherwise = problem $ "illegal tag " ++ show w

default_rgs :: T.Text
default_rgs = "PhiXA\tPhiA\tPhiA\nPhiXC\tPhiC\tPhiC\nPhiXG\tPhiG\tPhiG\nPhiXT\tPhiT\tPhiT\nPhiX\tPhiX\tis4\n"

-- | Compute mismatch score: sum of the qualities in 'a' at positions
-- where the bases don't match.  Works by comparing through an xor,
-- building a mask from it, then adding quality scores sideways.
--
-- Since we keep quality scores in the lower 5 bits of each byte, adding
-- all eight is guaranteed to fit into the highest 8 bits.
match :: Index -> Index -> Word64
match (Index a) (Index b) = score
  where x = a `xor` b
        y = (shiftR x 5 .|. shiftR x 6 .|. shiftR x 7) .&. 0x0101010101010101
        bitmask = (0x2020202020202020 - y) .&. 0x1F1F1F1F1F1F1F1F
        score = shiftR ((a .&. bitmask) * 0x0101010101010101) 56

-- | A mixture description is one probability for each combination of p7
-- and p5 index.  They should sum to one.
type Mix = VS.Vector Double
type MMix = VSM.IOVector Double

padding :: Int
padding = 31

stride' :: Int -> Int
stride' n5 = (n5 + padding) .&. complement padding

-- | Computing the naively assumed mix when nothing is known:  uniform
-- distribution.
naiveMix :: (Int,Int) -> Int -> Mix
naiveMix (n7,n5) total = VS.replicate vecsize (fromIntegral total / fromIntegral bins)
  where
    !vecsize = n7 * stride' n5
    !bins    = n7 * n5

-- | Matches an index against both p7 and p5 lists, computes posterior
-- likelihoods from the provided prior and accumulates them onto the
-- posterior.
unmix1 :: VS.Vector Index -> VS.Vector Index -> Mix -> MMix -> P Index Index -> IO ()
unmix1 p7 p5 prior acc (P x y) =
    let !m7 = VS.fromListN (VS.length p7) . map (phredPow . match x) $ VS.toList p7
        !l5 = stride' (VS.length p5)
        !m5 = VS.fromListN l5 $ map (phredPow . match y) (VS.toList p5) ++ repeat 0

    -- *sigh*, Vector doesn't fuse well.  Gotta hand it over to gcc.  :-(
    in VSM.unsafeWith acc                                           $ \pw ->
       VS.unsafeWith prior                                          $ \pv ->
       VS.unsafeWith m7                                             $ \q7 ->
       VS.unsafeWith m5                                             $ \q5 ->
       c_unmix_total pv q7 (fromIntegral $ VS.length m7)
                        q5 (fromIntegral $ l5 `div` succ padding)
                        nullPtr nullPtr                           >>= \total ->
       c_unmix_qual pw pv q7 (fromIntegral $ VS.length m7)
                          q5 (fromIntegral $ l5 `div` succ padding)
                          total 0 0                               >>= \_qual ->
       return ()    -- the quality is meaningless here

foreign import ccall unsafe "c_unmix_total"
    c_unmix_total :: Ptr Double                     -- prior mix
                  -> Ptr Double -> CUInt            -- P7 scores, length
                  -> Ptr Double -> CUInt            -- P5 scores, length/32
                  -> Ptr CUInt -> Ptr CUInt         -- out: ML P7 index, P5 index
                  -> IO Double                      -- total likelihood

foreign import ccall unsafe "c_unmix_qual"
    c_unmix_qual :: Ptr Double                      -- posterior mix, mutable accumulator
                 -> Ptr Double                      -- prior mix
                 -> Ptr Double -> CUInt             -- P7 scores, length
                 -> Ptr Double -> CUInt             -- P5 scores, length/32
                 -> Double                          -- total likelihood
                 -> CUInt -> CUInt                  -- maximizing P7 index, P5 index
                 -> IO Double                       -- posterior probability for any other assignment

-- | Matches an index against both p7 and p5 lists, computes MAP
-- assignment and quality score.
class1 :: HM.HashMap (Int,Int) (B.ByteString, VSM.IOVector Double)
       -> VS.Vector Index -> VS.Vector Index
       -> Mix -> (Index, Index) -> IO (Double, Int, Int)
class1 rgs p7 p5 prior (x,y) =
    let !m7 = VS.fromListN (VS.length p7) . map (phredPow . match x) $ VS.toList p7
        !l5 = stride' (VS.length p5)
        !m5 = VS.fromListN l5 $ map (phredPow . match y) (VS.toList p5) ++ repeat 0

    -- *sigh*, Vector doesn't fuse well.  Gotta hand it over to gcc.  :-(
    in alloca                                                       $ \pi7 ->
       alloca                                                       $ \pi5 ->
       VS.unsafeWith prior                                          $ \pv ->
       VS.unsafeWith m7                                             $ \q7 ->
       VS.unsafeWith m5                                             $ \q5 ->
       ( {-# SCC "c_unmix_total" #-}
         c_unmix_total pv q7 (fromIntegral $ VS.length m7)
                          q5 (fromIntegral $ l5 `div` succ padding)
                          pi7 pi5 )                               >>= \total ->
       peek pi7                                                   >>= \i7 ->
       peek pi5                                                   >>= \i5 ->
       withDirt (fromIntegral i7, fromIntegral i5)                  $ \pw ->
       ( {-# SCC "c_unmix_qual" #-}
         c_unmix_qual pw pv q7 (fromIntegral $ VS.length m7)
                            q5 (fromIntegral $ l5 `div` succ padding)
                            total i7 i5 )                         >>= \qual ->
       return ( qual, fromIntegral i7, fromIntegral i5 )
  where
    withDirt ix k = case HM.lookup ix rgs of
            Just (_,dirt) -> VSM.unsafeWith dirt k
            Nothing       -> k nullPtr


phredPow :: Word64 -> Double
phredPow x = exp $ -0.1 * log 10 * fromIntegral x

-- | One iteration of the EM algorithm.  Input is a vector of pairs of
-- indices, the p7 and p5 index collections, and a prior mixture; output
-- is the posterior mixture.
iterEM :: VS.Vector (P Index Index) -> VS.Vector Index -> VS.Vector Index -> Mix -> IO Mix
iterEM pps p7 p5 prior = do
    acc <- VSM.replicate (VS.length prior) 0
    VS.mapM_ (unmix1 p7 p5 prior acc) pps
    VS.unsafeFreeze acc

data Loudness = Quiet | Normal | Loud

unlessQuiet :: Monad m => Loudness -> m () -> m ()
unlessQuiet Quiet _ = return ()
unlessQuiet     _ k = k

whenLoud :: Monad m => Loudness -> m () -> m ()
whenLoud Loud k = k
whenLoud    _ _ = return ()

-- should I have a config for merging here?  adapter lists?
-- does it ever make sense to skip the merging?
data Conf = Conf {
        cf_index_list :: FilePath,
        cf_output     :: Maybe (BamMeta -> Stream (Of BamRec) LIO () -> LIO ()),
        cf_stats_hdl  :: Handle,
        cf_num_stats  :: Int -> Int,
        cf_threshold  :: Double,
        cf_loudness   :: Loudness,
        cf_single     :: Bool,
        cf_samplesize :: Int,
        cf_readgroups :: [FilePath],
        cf_implied    :: [T.Text],
        cf_merge      :: Maybe ([VS.Vector Nucleotides], [VS.Vector Nucleotides]) }

defaultConf :: IO Conf
defaultConf = do ixdb <- getDataFileName "index_db.json"
                 return $ Conf {
                        cf_index_list = ixdb,
                        cf_output     = Nothing,
                        cf_stats_hdl  = stdout,
                        cf_num_stats  = \l -> max 20 $ l * 5 `div` 4,
                        cf_threshold  = 0.000005,
                        cf_loudness   = Normal,
                        cf_single     = False,
                        cf_samplesize = 50000,
                        cf_readgroups = [],
                        cf_implied    = [default_rgs],
                        cf_merge      = Nothing }

options :: [OptDescr (Conf -> IO Conf)]
options = [
    Option "o" ["output"]          (ReqArg set_output   "FILE") "Send output to FILE",
    Option "I" ["index-database"]  (ReqArg set_index_db "FILE") "Read index database from FILE",
    Option "r" ["read-groups"]     (ReqArg set_rgs      "FILE") "Read read group definitions from FILE",
    Option "s" ["single-index"]    (NoArg           set_single) "Only consider first index",
    Option [ ] ["threshold"]       (ReqArg set_thresh   "FRAC") "Iterate till uncertainty is below FRAC",
    Option [ ] ["sample"]          (ReqArg set_sample    "NUM") "Sample NUM reads for mixture estimation",
    Option [ ] ["components"]      (ReqArg set_compo     "NUM") "Print NUM components of the mixture",
    Option [ ] ["nocontrol"]       (NoArg       set_no_control) "Suppress implied read groups for controls",
    Option [ ] ["default-adapters"](NoArg      set_default_ads) "Look for the default adapters",
    Option "F" ["forward-adapter"] (ReqArg set_forward   "SEQ") "SEQ is a possible forward adapter",
    Option "R" ["reverse-adapter"] (ReqArg set_reverse   "SEQ") "SEQ is a possible reverse adapter",
    Option "v" ["verbose"]         (NoArg             set_loud) "Print more diagnostic messages",
    Option "q" ["quiet"]           (NoArg            set_quiet) "Print fewer diagnostic messages",
    Option "h?"["help", "usage"]   (NoArg        $ const usage) "Print this message and exit",
    Option "V" ["version"]         (NoArg        $  const vrsn) "Display version number and exit" ]
  where
    set_output  "-" c = return $ c { cf_output = Just $ pipeBamOutput, cf_stats_hdl = stderr }
    set_output   fp c = return $ c { cf_output = Just $ writeBamFile fp }
    set_index_db fp c = return $ c { cf_index_list = fp }
    set_rgs      fp c = return $ c { cf_readgroups = fp : cf_readgroups c }
    set_loud        c = return $ c { cf_loudness = Loud }
    set_quiet       c = return $ c { cf_loudness = Quiet }
    set_single      c = return $ c { cf_single = True }
    set_no_control  c = return $ c { cf_implied = [] }
    set_thresh    a c = readIO a >>= \x -> return $ c { cf_threshold = x }
    set_sample    a c = readIO a >>= \x -> return $ c { cf_samplesize = x }
    set_compo     a c = readIO a >>= \x -> return $ c { cf_num_stats = const x }

    with_merge    f c = return $ c { cf_merge = Just . f . fromMaybe ([],[]) $ cf_merge c }
    set_forward   a c = readIO a >>= \x -> with_merge (first  (x:)) c
    set_reverse   a c = readIO a >>= \x -> with_merge (second (x:)) c
    set_default_ads c = with_merge ((++) default_fwd_adapters *** (++) default_rev_adapters) c

    usage = do pn <- getProgName
               putStrLn $ usageInfo ("Usage: " ++ pn ++ " [options] [bam-files]\n" ++
                                     "Decomposes a mix of libraries and assigns read groups.") options
               exitSuccess

    vrsn = do pn <- getProgName
              hPutStrLn stderr $ pn ++ ", version " ++ showVersion version
              exitSuccess


adj_left :: Int -> Char -> L.Builder -> L.Builder
adj_left n c b = mconcat (replicate (n - fromIntegral (L.length t)) (L.singleton c)) <> L.fromLazyText t
  where t = L.toLazyText b

adj_left_text :: Int -> Char -> T.Text -> L.Builder
adj_left_text n c t = mconcat (replicate (n - T.length t) (L.singleton c)) <> L.fromText t

main :: IO ()
main = do
  (opts, files, errs) <- getOpt Permute options <$> getArgs
  unless (null errs) $ mapM_ (hPutStrLn stderr) errs >> exitFailure
  Conf{..} <- foldl (>>=) defaultConf opts
  when (null files) $ hPutStrLn stderr "no input files." >> exitFailure
  add_pg <- addPG $ Just version

  let (plevel, wlevel) = case cf_loudness of Quiet -> (Error,Error) ; Normal -> (Warning,Warning) ; Loud -> (Info,Warning)
  withLogging_ (LoggingConf plevel wlevel Error 10 True) $ do
    Both{..} <- liftIO (B.readFile cf_index_list) >>= \raw -> case decodeStrict' raw of
                    Nothing -> panic "Couldn't parse index database."
                    Just  x | cf_single -> return $ x { p5is = single_placeholder }
                            | otherwise -> return   x

    rgdefs <- readRGdefns (alias_names p7is) (alias_names p5is) . T.concat . (++) cf_implied =<<
              liftIO (mapM T.readFile cf_readgroups)
    logStringLn $ "Got " ++ showNum (VS.length (unique_indices p7is)) ++ " unique P7 indices and "
                         ++ showNum (VS.length (unique_indices p5is)) ++ " unique P5 indices."
    logStringLn $ "Declared " ++ showNum (length rgdefs) ++ " read groups."

    let n7     = VS.length $ unique_indices p7is
        n5     = VS.length $ unique_indices p5is
        stride = stride' n5
        vsize  = n7 * stride

    !rgs <- foldM (\hm (RG !rg !i7 !i5 _) ->
                     case HM.lookup (i7,i5) hm of
                         Nothing      -> do dirt <- liftIO $ VSM.replicate vsize (0::Double)
                                            pure $ HM.insert (i7,i5) (rg,dirt) hm
                         Just (rg',_) -> panic $ "Read groups " ++ show rg' ++ " and "
                                                 ++ show rg ++ " have the same indices."
                   ) HM.empty rgdefs

    let inspect1 = inspect' rgs (canonical_names p7is) (canonical_names p5is)

    ixvec <- concatInputs files $ gather cf_samplesize
    logStringLn $ "Got " ++ showNum (VS.length ixvec) ++ " index pairs."

    let go !n v = do logString_ $ "decomposing mix " ++ replicate n '.'
                     v' <- liftIO $ iterEM ixvec (unique_indices p7is) (unique_indices p5is) v
                     liftIO $ whenLoud cf_loudness $ hPutStrLn stderr [] >> inspect1 stderr 20 v'
                     let d = VS.foldl' (\a -> max a . abs) 0 $ VS.zipWith (-) v v'
                     if n < 50 && d > cf_threshold * fromIntegral (VS.length ixvec)
                          then go (n+1) v'
                          else do logString_ []
                                  logStringLn (if n == 0 then "maximum number of iterations reached."
                                                         else "mixture ratios converged.")
                                  return v'

    mix <- go (0::Int) $ naiveMix (VS.length $ unique_indices p7is, VS.length $ unique_indices p5is) (VS.length ixvec)

    unlessQuiet cf_loudness . liftIO $ do
            T.hPutStrLn cf_stats_hdl "\nfinal mixture estimate:"
            inspect1 cf_stats_hdl (cf_num_stats $ HM.size rgs) mix

    let maxlen = maximum $ map (B.length . rgid) rgdefs
        ns7 = canonical_names p7is
        ns5 = canonical_names p5is
        num = 7


    let withMerge k = case cf_merge of
            Nothing           -> k id
            Just (fads, rads) -> withADSeqs fads $ \f ->
                                 withADSeqs rads $ \r ->
                                 k (mergeTrimBam 20 200 f r)
    case cf_output of
        Nothing  -> do  unlessQuiet cf_loudness $ liftIO $ do
                            T.hPutStrLn cf_stats_hdl "\nmaximum achievable quality, top pollutants:"
                            forM_ (sortOn (fst.snd) $ HM.toList rgs) $ \((i7,i5), (rgid,_)) -> do
                                (p,_,_) <- class1 HM.empty (unique_indices p7is) (unique_indices p5is) mix
                                                  (unique_indices p7is VS.! i7, unique_indices p5is VS.! i5)

                                let qmax = negate . round $ 10 / log 10 * log p :: Int
                                L.hPutStrLn cf_stats_hdl . L.toLazyText $
                                        adj_left_text maxlen ' ' (T.decodeUtf8 rgid) <>
                                        L.fromText ": " <>
                                        adj_left 4 ' ' (L.singleton 'Q' <> L.decimal (max 0 qmax))

        Just out -> do  withMerge $ \do_merge ->
                          concatInputs files $ \hdr ->
                            let hdr' = hdr {meta_other_shit =
                                              [ os | os@(k,_) <- meta_other_shit hdr, k /= "RG" ] ++
                                              HM.elems (HM.fromList [(rgid,("RG",("ID",rgid):tags)) | RG{..} <- rgdefs])}
                            in out (add_pg hdr') .
                               progressNum "writing " 0x100000 .
                               do_merge .
                               Q.mapM (\br -> do
                                    let b = unpackBam br
                                        eff_rgs | not (isPaired b) = rgs
                                                | isFirstMate b    = rgs
                                                | otherwise        = HM.empty
                                    (p,i7,i5) <- liftIO $ class1 eff_rgs (unique_indices p7is) (unique_indices p5is) mix
                                                                         (fromTags "XI" "YI" br, fromTags "XJ" "YJ" br)
                                    let q = negate . round $ 10 / log 10 * log p
                                        ex = deleteE "ZR" . deleteE "Z0" . deleteE "Z2" . updateE "Z1" (Int q) $
                                             updateE "ZX" (Text $ T.encodeUtf8 $ T.concat [ ns7 V.! i7, ",", ns5 V.! i5 ]) $
                                             case HM.lookup (i7,i5) rgs of
                                               Nothing      -> deleteE "RG" $ b_exts b
                                               Just (rgn,_) -> updateE "RG" (Text rgn) $ b_exts b
                                    return $ case lookup "ZQ" ex of
                                        Just (Text t) | BS.null t' -> b { b_exts = deleteE "ZQ" ex
                                                                        , b_flag = b_flag b .&. complement flagFailsQC }
                                                      | otherwise  -> b { b_exts = updateE "ZQ" (Text t') ex }
                                          where
                                            t' = BS.filter (\c -> c /= 'C' && c /= 'I' && c /= 'W') t
                                        _                          -> b { b_exts = ex
                                                                        , b_flag = b_flag b .&. complement flagFailsQC })

                        unlessQuiet cf_loudness . liftIO $ do
                            grand_total <- foldM (\ !acc (_,dirt) -> (+) acc . VS.sum <$> VS.freeze dirt) 0 rgs
                            T.hPutStrLn cf_stats_hdl "\nmaximum achievable and average quality, top pollutants:"
                            forM_ (sortOn (fst.snd) $ HM.toList rgs) $ \((i7,i5), (rgid,dirt_)) -> do
                                dirt <- VS.freeze dirt_
                                (p,_,_) <- class1 HM.empty (unique_indices p7is) (unique_indices p5is) mix
                                                  (unique_indices p7is VS.! i7, unique_indices p5is VS.! i5)

                                let total  = VS.sum dirt
                                    others = VS.sum $ VS.ifilter (\i _ -> i /= i7 * stride + i5) dirt
                                    qmax = negate . round $ 10 / log 10 * log p :: Int
                                    qavg = negate . round $ 10 / log 10 * log (others/total) :: Int

                                let v' = VS.create $ do v <- VSM.unsafeNew (VS.length dirt)
                                                        VG.imapM_ (\i a -> VSM.write v i (P i a)) dirt
                                                        V.sortBy (\(P _ a) (P _ b) -> compare b a) v
                                                        pure v

                                let fmt_one (P i n) =
                                        let (i7', i5') = i `quotRem` stride
                                            chunk = L.formatRealFloat L.Fixed (Just 2) (100*n/total) <> L.singleton '%' <>
                                                    L.singleton ' ' <> L.fromText (ns7 V.! i7') <>
                                                    L.singleton '/' <> L.fromText (ns5 V.! i5') <>
                                                    case HM.lookup (i7',i5') rgs of
                                                        Nothing     -> mempty
                                                        Just (rg,_) -> L.singleton ' ' <> L.singleton '(' <>
                                                                       L.fromText (T.decodeUtf8 rg) <> L.singleton ')'
                                        in if (i7 == i7' && i5 == i5') || i5' >= n5 then id else (:) chunk

                                when (total >= 1) . L.hPutStrLn cf_stats_hdl . L.toLazyText $
                                        adj_left_text maxlen ' ' (T.decodeUtf8 rgid) <>
                                        L.singleton ':' <> L.singleton ' ' <>
                                        adj_left 4 ' ' (L.singleton 'Q' <> L.decimal (max 0 qmax)) <> L.fromText ", " <>
                                        adj_left 4 ' ' (L.singleton 'Q' <> L.decimal (max 0 qavg)) <> L.fromText ", " <>
                                        L.fromString (showNum (round total :: Int)) <> L.fromText " (" <>
                                        L.formatRealFloat L.Fixed (Just 2) (100*total/grand_total) <> L.fromText "%); " <>
                                        foldr1 (\a b -> a <> L.fromText ", " <> b)
                                            (take num $ VS.foldr fmt_one [] v')

inspect' :: HM.HashMap (Int,Int) (B.ByteString, t) -> V.Vector T.Text -> V.Vector T.Text -> Handle -> Int -> Mix -> IO ()
inspect' rgs n7 n5 hdl num_ mix = do
    -- Due to padding, we get invalid indices here.  Better filter them
    -- out, because we sure can't print them later.
    let num = min num_ $ V.length n5 * V.length n7
    v <- VS.unsafeThaw $ VS.fromListN (V.length n5 * V.length n7) $
                filter (\(P i _) -> i `rem` stride' (V.length n5) < V.length n5) $
                zipWith P [0..] $ VS.toList mix
    V.partialSortBy (\(P _ a) (P _ b) -> compare b a) v num         -- meh.
    v' <- VS.unsafeFreeze v

    let total  = VS.foldl' (\a (P _ b) -> a+b) 0 $ v'
        others = VS.foldl' (\a (P _ b) -> a+b) 0 $ VS.drop num v'

    VS.forM_ (VS.take num v') $ \(P i n) -> do
       let (i7, i5) = i `quotRem` stride' (V.length n5)
       L.hPutStrLn hdl . L.toLazyText $
            adj_left_text 7 ' ' (n7 V.! i7) <> L.singleton ',' <> L.singleton ' ' <>
            adj_left_text 7 ' ' (n5 V.! i5) <> L.singleton ':' <> L.singleton ' ' <>
            adj_left 8 ' ' (L.formatRealFloat L.Fixed (Just 3) (100 * n / total)) <> L.singleton '%' <> L.singleton ' ' <>
            case HM.lookup (i7,i5) rgs of
                Nothing     -> mempty
                Just (rg,_) -> L.singleton '(' <> L.fromText (T.decodeUtf8 rg) <> L.singleton ')'

    L.hPutStrLn hdl . L.toLazyText $
        L.fromLazyText "      all others: " <>
        adj_left 8 ' ' (L.formatRealFloat L.Fixed (Just 3) (100 * others / total)) <>
        L.singleton '%'


data Widow = Widow Bytes deriving (Typeable, Show)
instance Exception Widow where
    displayException (Widow nm) =
        "Widow (paired read without mate) found: " ++ show nm

mergeTrimBam :: Int -> Int -> AD_Seqs -> AD_Seqs
             -> Stream (Of BamRec) LIO r -> Stream (Of BamRec) LIO r
mergeTrimBam lowq highq fwd_ads rev_ads = go1
  where
    go1 = lift . inspect >=> \case
        Left         r                -> pure r
        Right (r1 :> s) | isPaired r1 -> go2 r1 s
                        | otherwise   -> liftIO (trimBam lowq highq fwd_ads r1) >>= each >> go1 s

    go2 r1 = lift . inspect >=> \case
        Left         r                 -> lift (logMsg Warning (Widow (b_qname r1))) >> pure r
        Right (r2 :> s)
            | not (isPaired r2)        -> liftIO (trimBam lowq highq fwd_ads r2) >>= each >> go2 r1 s
            | b_qname r1 == b_qname r2 -> liftIO (mergeBam lowq highq fwd_ads rev_ads r1 r2) >>= each >> go1 s
            | otherwise                -> lift (logMsg Warning (Widow (b_qname r1))) >> go2 r2 s

